Global field
============

Provides a field that displays a global variable text value.

Have you ever needed to display a bit of text that would always be the same
across your whole site? Wouldn't it be nice to display that as a field on a
Taxonomy term or Node? This is exactly what Global field does.
